﻿var data = [
        { id: "id001", name: "Tangela", type1: "Ground", type2: "Fighting", creator: "/u/winkwright" },
        { id: "id002", name: "Tangrowth", type1: "Ground", type2: "Fighting", creator: "/u/winkwright" },
        { id: "id003", name: "Exeggcute", type1: "Dark", type2: "", creator: "/u/yungbro" },
        { id: "id004", name: "Exeggutor", type1: "Dark", type2: "", creator: "/u/yungbro" },
        { id: "id005", name: "Plusle", type1: "Fire", type2: "Fairy", creator: "/u/powerpow365" },
        { id: "id006", name: "Minun", type1: "Ice", type2: "Fairy", creator: "/u/powerpow365" },
        { id: "id007", name: "Solosis", type1: "Ghost", type2: "Rock", creator: "/u/Steads42" },
        { id: "id008", name: "Duosion", type1: "Ghost", type2: "Rock", creator: "/u/Steads42" },
        { id: "id009", name: "Reuniclus", type1: "Ghost", type2: "Rock", creator: "/u/Steads42" },
        { id: "id010", name: "Clamperl", type1: "Dragon", type2: "", creator: "/u/yungbro" },
        { id: "id011", name: "Huntail", type1: "Dragon", type2: "Dark", creator: "/u/yungbro" },
        { id: "id012", name: "Gorebyss", type1: "Fairy", type2: "Dragon", creator: "/u/yungbro" },
        { id: "id013", name: "Tyrogue", type1: "Water", type2: "", creator: "/u/Vlieking" },
        { id: "id014", name: "Hitmonlee", type1: "Fire", type2: "Water", creator: "/u/Vlieking" },
        { id: "id015", name: "Hitmonchan", type1: "Psychic", type2: "Water", creator: "/u/Vlieking" },
        { id: "id016", name: "Hitmontop", type1: "Poison", type2: "Water", creator: "/u/Vlieking" },
        { id: "id017", name: "Drifloon", type1: "Fire", type2: "Flying", creator: "/u/yungbro" },
        { id: "id018", name: "Drifblim", type1: "Fire", type2: "Flying", creator: "/u/yungbro" },
        { id: "id019", name: "Mareep", type1: "Dark", type2: "Dragon", creator: "/u/PokeNerd123" },
        { id: "id020", name: "Flaaffy", type1: "Dark", type2: "Dragon", creator: "/u/PokeNerd123" },
        { id: "id021", name: "Ampharos", type1: "Dark", type2: "Dragon", creator: "/u/PokeNerd123" },
        { id: "id021m", name: "Ampharos", type1: "Dark", type2: "Dragon", creator: "/u/PokeNerd123" },
        { id: "id022", name: "Skarmory", type1: "Dark", type2: "Grass", creator: "/u/StarryD" },
        { id: "id023", name: "Baltoy", type1: "Electric", type2: "", creator: "/u/Achromobacter" },
        { id: "id024", name: "Claydol", type1: "Electric", type2: "", creator: "/u/Achromobacter" },
        { id: "id025", name: "Petilil", type1: "Fire", type2: "Water", creator: "/u/Steads42" },
        { id: "id026", name: "Lilligant", type1: "Fire", type2: "Water", creator: "/u/Steads42" },
        { id: "id027", name: "Petilil", type1: "Fairy", type2: "Flying", creator: "/u/shadowsofme" },
        { id: "id028", name: "Lilligant", type1: "Fairy", type2: "Flying", creator: "/u/shadowsofme" },
        { id: "id029", name: "Swablu", type1: "Grass", type2: "Flying", creator: "/u/Draexzhan" },
        { id: "id030", name: "Altaria", type1: "Grass", type2: "Flying", creator: "/u/Draexzhan" },
        { id: "id031", name: "Dratini", type1: "Electric", type2: "Water", creator: "/u/PokeNerd123" },
        { id: "id032", name: "Dragonair", type1: "Electric", type2: "Water", creator: "/u/PokeNerd123" },
        { id: "id033", name: "Dragonite", type1: "Electric", type2: "Water", creator: "/u/PokeNerd123" },
        { id: "id034", name: "Timburr", type1: "Dark", type2: "Fairy", creator: "/u/PokeNerd123" },
        { id: "id035", name: "Gurdurr", type1: "Dark", type2: "Fairy", creator: "/u/PokeNerd123" },
        { id: "id036", name: "Conkeldurr", type1: "Dark", type2: "Fairy", creator: "/u/PokeNerd123" },
        { id: "id037", name: "Aron", type1: "Fire", type2: "Steel", creator: "/u/Draexzhan" },
        { id: "id038", name: "Lairon", type1: "Fire", type2: "Steel", creator: "/u/Draexzhan" },
        { id: "id039", name: "Aggron", type1: "Fire", type2: "Steel", creator: "/u/Draexzhan" },
        { id: "id040", name: "Swinub", type1: "Electric", type2: "Ice", creator: "/u/winkwright" },
        { id: "id041", name: "Piloswine", type1: "Electric", type2: "Ice", creator: "/u/winkwright" },
        { id: "id042", name: "Mamoswine", type1: "Electric", type2: "Ice", creator: "/u/winkwright" },
        { id: "id043", name: "Magby", type1: "Water", type2: "", creator: "/u/Ubodot" },
        { id: "id044", name: "Magmar", type1: "Water", type2: "", creator: "/u/Ubodot" },
        { id: "id045", name: "Magmortar", type1: "Water", type2: "Steel", creator: "/u/Ubodot" },
        { id: "id046", name: "Venipede", type1: "Rock", type2: "Fire", creator: "/u/DankRabbit" },
        { id: "id047", name: "Whirlipede", type1: "Rock", type2: "Fire", creator: "/u/DankRabbit" },
        { id: "id048", name: "Scolipede", type1: "Rock", type2: "Fire", creator: "/u/DankRabbit" },
        { id: "id049", name: "Chinchou", type1: "Ghost", type2: "Fire", creator: "/u/CommanderPhoenix" },
        { id: "id050", name: "Lanturn", type1: "Ghost", type2: "Fire", creator: "/u/CommanderPhoenix" },
        { id: "id051", name: "Trapinch", type1: "Bug", type2: "", creator: "/u/PCTheSkitty" },
        { id: "id052", name: "Vibrava", type1: "Bug", type2: "Fairy", creator: "/u/PCTheSkitty" },
        { id: "id053", name: "Flygon", type1: "Bug", type2: "Fairy", creator: "/u/PCTheSkitty" },
        { id: "id054", name: "Kabuto", type1: "Bug", type2: "Dark", creator: "/u/PokeNerd123" },
        { id: "id055", name: "Kabutops", type1: "Bug", type2: "Dark", creator: "/u/PokeNerd123" },
        { id: "id056", name: "Ralts", type1: "Fighting", type2: "Normal", creator: "/u/PCTheSkitty" },
        { id: "id057", name: "Kirlia", type1: "Fighting", type2: "Normal", creator: "/u/PCTheSkitty" },
        { id: "id058", name: "Gardevoir", type1: "Fighting", type2: "Normal", creator: "/u/PCTheSkitty" },
        { id: "id059", name: "Gallade", type1: "Fighting", type2: "Dark", creator: "/u/PCTheSkitty" },
        { id: "id060", name: "Yamask", type1: "Steel", type2: "Poison", creator: "/u/Palfi" },
        { id: "id061", name: "Cofagrigus", type1: "Steel", type2: "Poison", creator: "/u/Palfi" },
        { id: "id062", name: "Luvdisc-Spades", type1: "Psychic", type2: "Dark", creator: "/u/Palfi" },
        { id: "id062_2", name: "Luvdisc-Hearts", type1: "Psychic", type2: "Dark", creator: "/u/Palfi" },
        { id: "id062_3", name: "Luvdisc-Diamonds", type1: "Psychic", type2: "Dark", creator: "/u/Palfi" },
        { id: "id062_4", name: "Luvdisc-Clubs", type1: "Psychic", type2: "Dark", creator: "/u/Palfi" },
        { id: "id063", name: "Throh", type1: "Normal", type2: "Fighting", creator: "/u/potatoking12_" },
        { id: "id064", name: "Leafeon", type1: "Rock", type2: "", creator: "/u/Aboodie" },
        { id: "id065", name: "Dunsparce", type1: "Ground", type2: "", creator: "/u/PokeNerd123" },
        { id: "id066", name: "Zubat", type1: "Ice", type2: "Flying", creator: "/u/AdmaDeadwolf" },
        { id: "id067", name: "Golbat", type1: "Ice", type2: "Flying", creator: "/u/AdmaDeadwolf" },
        { id: "id068", name: "Crobat", type1: "Ice", type2: "Flying", creator: "/u/AdmaDeadwolf" },
        { id: "id069", name: "Mankey", type1: "Psychic", type2: "Flying", creator: "/u/Zazulitao" },
        { id: "id070", name: "Primeape", type1: "Psychic", type2: "Flying", creator: "/u/Zazulitao" },
        { id: "id071", name: "Torkoal", type1: "Poison", type2: "Steel", creator: "/u/Zazulitao" },
        { id: "id072", name: "Diglett", type1: "Ice", type2: "Normal", creator: "/u/Zazulitao" },
        { id: "id073", name: "Dugtrio", type1: "Ice", type2: "Normal", creator: "/u/Zazulitao" },
        { id: "id073m", name: "Dugtrio", type1: "Ice", type2: "Normal", creator: "/u/Zazulitao" },
        { id: "id074", name: "Budew", type1: "Ice", type2: "Steel", creator: "/u/Steads42" },
        { id: "id075", name: "Roselia", type1: "Ice", type2: "Steel", creator: "/u/Steads42" },
        { id: "id076", name: "Roserade", type1: "Ice", type2: "Steel", creator: "/u/Steads42" },
        { id: "id077", name: "Pineco", type1: "Grass", type2: "Bug", creator: "/u/AdmaDeadwolf" },
        { id: "id078", name: "Forretress", type1: "Grass", type2: "Psychic", creator: "/u/AdmaDeadwolf" },
        { id: "id079", name: "Oddish", type1: "Fire", type2: "Rock", creator: "/u/Henriww" },
        { id: "id080", name: "Gloom", type1: "Fire", type2: "Rock", creator: "/u/Henriww" },
        { id: "id081", name: "Vileplume", type1: "Fire", type2: "Rock", creator: "/u/Henriww" },
        { id: "id082", name: "Bellossom", type1: "Fire", type2: "", creator: "/u/Henriww" },
        { id: "id083", name: "Heatmor", type1: "Poison", type2: "Steel", creator: "/u/Radryl" },
        { id: "id084", name: "Sylveon", type1: "Poison", type2: "", creator: "/u/Draexzhan" },
        { id: "id085", name: "Nidoran♀", type1: "Dragon", type2: "", creator: "/u/Draexzhan" },
        { id: "id086", name: "Nidorina", type1: "Dragon", type2: "", creator: "/u/Draexzhan" },
        { id: "id087", name: "Nidoqueen", type1: "Dragon", type2: "Fighting", creator: "/u/Draexzhan" },
        { id: "id088", name: "Nidoran♂", type1: "Dragon", type2: "", creator: "/u/Draexzhan" },
        { id: "id089", name: "Nidorino", type1: "Dragon", type2: "", creator: "/u/Draexzhan" },
        { id: "id090", name: "Nidoking", type1: "Dragon", type2: "Fighting", creator: "/u/Draexzhan" },
        { id: "id091", name: "Chikorita", type1: "Ice", type2: "Fire", creator: "/u/AgateMizuki" },
        { id: "id092", name: "Bayleef", type1: "Ice", type2: "Fire", creator: "/u/AgateMizuki" },
        { id: "id093", name: "Meganium", type1: "Ice", type2: "Fire", creator: "/u/AgateMizuki" },
        { id: "id094", name: "Sneasel", type1: "Ghost", type2: "Steel", creator: "/u/Locaruoti" },
        { id: "id095", name: "Weavile", type1: "Ghost", type2: "Steel", creator: "/u/Locaruoti" },
        { id: "id096", name: "Carnivine", type1: "Dragon", type2: "Fire", creator: "/u/Natias257" },
        { id: "id097", name: "Feebas", type1: "Ghost", type2: "", creator: "/u/1ofthe4rocketbros" },
        { id: "id098", name: "Milotic", type1: "Ghost", type2: "", creator: "/u/1ofthe4rocketbros" },
        { id: "id099", name: "Lotad", type1: "Psychic", type2: "Water", creator: "/u/Kalorian" },
        { id: "id100", name: "Lombre", type1: "Psychic", type2: "Water", creator: "/u/Kalorian" },
        { id: "id101", name: "Ludicolo", type1: "Psychic", type2: "Water", creator: "/u/Kalorian" },
        { id: "id102", name: "Qwilfish", type1: "Steel", type2: "Fighting", creator: "/u/potatoking12_" },
        { id: "id103", name: "Aipom", type1: "Ghost", type2: "", creator: "/u/xZachy" },
        { id: "id104", name: "Ambipom", type1: "Ghost", type2: "", creator: "/u/xZachy" },
        { id: "id105", name: "Sableye", type1: "Fire", type2: "Rock", creator: "/u/SketchyGamer" },
        { id: "id105m", name: "Sableye", type1: "Fire", type2: "Rock", creator: "/u/SketchyGamer" },
        { id: "id106", name: "Treecko", type1: "Normal", type2: "Fighting", creator: "/u/xZachy" },
        { id: "id107", name: "Grovyle", type1: "Normal", type2: "Fighting", creator: "/u/xZachy" },
        { id: "id108", name: "Sceptile", type1: "Dragon", type2: "Fighting", creator: "/u/xZachy" }, 
        { id: "id109", name: "Dedenne", type1: "Dark", type2: "", creator: "/u/Natias257" },
        { id: "id110", name: "Magikarp", type1: "Steel", type2: "", creator: "/u/Aboodie" },
        { id: "id111", name: "Gyarados", type1: "Steel", type2: "Dragon", creator: "/u/Aboodie" },
        { id: "id112", name: "Abra", type1: "Fire", type2: "Water", creator: "/u/Aboodie" },
        { id: "id113", name: "Kadabra", type1: "Fire", type2: "Water", creator: "/u/Aboodie" },
        { id: "id114", name: "Alakazam", type1: "Fire", type2: "Water", creator: "/u/Aboodie" },
        { id: "id114m", name: "Alakazam", type1: "Fire", type2: "Water", creator: "/u/Aboodie" },
        { id: "id115", name: "Dwebble", type1: "Poison", type2: "Grass", creator: "/u/MorallyIncorrect" },
        { id: "id116", name: "Crustle", type1: "Poison", type2: "Grass", creator: "/u/MorallyIncorrect" },
        { id: "id117", name: "Bidoof", type1: "Ice", type2: "", creator: "/u/MorallyIncorrect" },
        { id: "id118", name: "Bibarel", type1: "Ice", type2: "", creator: "/u/MorallyIncorrect" },
        { id: "id119", name: "Amaura", type1: "Grass", type2: "Water", creator: "/u/Steads42" },
        { id: "id120", name: "Aurorus", type1: "Grass", type2: "Water", creator: "/u/Steads42" },
        { id: "id121", name: "Spiritomb", type1: "Water", type2: "Ground", creator: "/u/powerpow365" },
        { id: "id122", name: "Articuno", type1: "Psychic", type2: "", creator: "/u/PCTheSkitty" },
        { id: "id123", name: "Zapdos", type1: "Dark", type2: "", creator: "/u/PCTheSkitty" },
        { id: "id124", name: "Riolu", type1: "Dark", type2: "", creator: "/u/Ubodot" },
        { id: "id125", name: "Lucario", type1: "Dark", type2: "Ground", creator: "/u/Ubodot" },
        { id: "id125m", name: "Lucario", type1: "Dark", type2: "Ground", creator: "/u/Ubodot" },
        { id: "id126", name: "Goomy", type1: "Ground", type2: "Water", creator: "/u/AChosen_Username" },
        { id: "id127", name: "Sliggoo", type1: "Ground", type2: "Water", creator: "/u/AChosen_Username" },
        { id: "id128", name: "Goodra", type1: "Ground", type2: "Water", creator: "/u/AChosen_Username" },
        { id: "id129", name: "Elekid", type1: "Rock", type2: "", creator: "/u/Ubodot" },
        { id: "id130", name: "Electabuzz", type1: "Rock", type2: "", creator: "/u/Ubodot" },
        { id: "id131", name: "Electivire", type1: "Rock", type2: "Dark", creator: "/u/Ubodot" },
        { id: "id132", name: "Shuckle", type1: "Fighting", type2: "Steel", creator: "/u/Zazulitao" },
        { id: "id133", name: "Seedot", type1: "Electric", type2: "", creator: "/u/SketchyGamer" },
        { id: "id134", name: "Nuzleaf", type1: "Electric", type2: "", creator: "/u/SketchyGamer" },
        { id: "id135", name: "Shiftry", type1: "Electric", type2: "", creator: "/u/SketchyGamer" },
        { id: "id136", name: "Remoraid", type1: "Fire", type2:"", creator: "/u/Missingno_OP" },
        { id: "id137", name: "Octillery", type1: "Fire", type2:"", creator: "/u/Missingno_OP" },
        { id: "id138", name: "Seviper", type1: "Fire", type2: "Steel", creator: "/u/SketchyGamer" },
        { id: "id139", name: "Darkrai", type1: "Ghost", type2: "Normal", creator: "/u/Aboodie" },
        { id: "id140", name: "Joltik", type1: "Dark", type2: "", creator: "/u/DialgaTheTimeLord" },
        { id: "id141", name: "Galvantula", type1: "Dark", type2: "Poison", creator: "/u/DialgaTheTimeLord" },
        { id: "id142", name: "Bronzor", type1: "Ghost", type2: "Steel", creator: "/u/Ubodot" },
        { id: "id143", name: "Bronzong", type1: "Ghost", type2: "Steel", creator: "/u/Ubodot" },
        { id: "id144", name: "Umbreon", type1: "Steel", type2: "", creator: "/u/Ubodot" }, 
        { id: "id145", name: "Darumaka", type1: "Ghost", type2: "Dark", creator: "/u/potatoking12_" },
        { id: "id146", name: "Darmanitan", type1: "Ghost", type2: "Dark", creator: "/u/potatoking12_" },
        { id: "id147", name: "Skitty", type1: "Grass", type2: "", creator: "/u/pokefan1241" },
        { id: "id148", name: "Delcatty", type1: "Grass", type2: "", creator: "/u/pokefan1241" },
        { id: "id149", name: "Chingling", type1: "Steel", type2: "Fire", creator: "/u/leafblade_forever" },
        { id: "id150", name: "Chimecho", type1: "Steel", type2: "Fire", creator: "/u/leafblade_forever" },
        { id: "id151", name: "Tentacool", type1: "Grass", type2: "Poison", creator: "/u/xxxHexxxx" },
        { id: "id152", name: "Tentacruel", type1: "Grass", type2: "Poison", creator: "/u/xxxHexxxx" },
        { id: "id153", name: "Croagunk", type1: "Fire", type2: "", creator: "/u/pokefan1241" },
        { id: "id154", name: "Toxicroak", type1: "Fire", type2: "", creator: "/u/pokefan1241" },
        { id: "id155", name: "Mawile", type1: "Grass", type2: "Dark", creator: "/u/spimission" },
        { id: "id155m", name: "Mawile", type1: "Grass", type2: "Dark", creator: "/u/spimission" },
        { id: "id156", name: "Jirachi", type1: "Fire", type2: "Fairy", creator: "/u/spimission" },
        { id: "id157", name: "Karrablast", type1: "Fairy", type2: "", creator: "/u/bradleyinabox" },
        { id: "id158", name: "Escavalier", type1: "Fairy", type2: "", creator: "/u/bradleyinabox" },
        { id: "id159", name: "Magikarp", type1: "Ice", type2: "Fire", creator: "/u/spimission" },
        { id: "id160", name: "Gyarados", type1: "Ice", type2: "Fire", creator: "/u/spimission" },        
        { id: "id161", name: "Carvanha", type1: "Dragon", type2: "Steel", creator: "/u/PokeNerd123" },
        { id: "id162", name: "Sharpedo", type1: "Dragon", type2: "Steel", creator: "/u/PokeNerd123" },
        { id: "id163", name: "Regirock", type1: "Ground", type2: "", creator: "/u/Lucci85" },
        { id: "id164", name: "Regice", type1: "Water", type2: "", creator: "/u/Lucci85" },
        { id: "id165", name: "Registeel", type1: "Fire", type2: "", creator: "/u/Lucci85" },
        { id: "id166", name: "Regigigas", type1: "Fighting", type2: "", creator: "/u/Lucci85" },
        { id: "id167", name: "Vaporeon", type1: "Poison", type2: "", creator: "/u/Lucci85" },
        { id: "id168", name: "Flareon", type1: "Flying", type2: "", creator: "/u/bradleyinabox" },
        { id: "id169", name: "Tyrogue", type1: "Dark", type2: "", creator: "/u/KazeBlack" },
        { id: "id170", name: "Hitmonlee", type1: "Dark", type2: "", creator: "/u/KazeBlack" },
        { id: "id171", name: "Hitmonchan", type1: "Dark", type2: "", creator: "/u/KazeBlack" },
        { id: "id172", name: "Hitmontop", type1: "Dark", type2: "", creator: "/u/KazeBlack" },
        { id: "id173", name: "Rattata", type1: "Ghost", type2: "", creator: "/u/KazeBlack" },
        { id: "id174", name: "Raticate", type1: "Ghost", type2: "Dragon", creator: "/u/KazeBlack" },
        { id: "id175", name: "Porygon", type1: "Water", type2: "Fairy", creator: "/u/xArcKnightx" },
        { id: "id176", name: "Porygon2", type1: "Water", type2: "Fairy", creator: "/u/xArcKnightx" },
        { id: "id177", name: "Porygon-Z", type1: "Water", type2: "Fairy", creator: "/u/xArcKnightx" },
        { id: "id178", name: "Froakie", type1: "Grass", type2: "", creator: "/u/Ubodot" },
        { id: "id179", name: "Frogadier", type1: "Grass", type2: "", creator: "/u/Ubodot" },
        { id: "id180", name: "Greninja", type1: "Grass", type2: "Fire", creator: "/u/Ubodot" },
        { id: "id181", name: "Horsea", type1: "Bug", type2: "Electric", creator: "/u/KazeBlack" },
        { id: "id182", name: "Seadra", type1: "Bug", type2: "Electric", creator: "/u/KazeBlack" },
        { id: "id183", name: "Kingdra", type1: "Bug", type2: "Electric", creator: "/u/KazeBlack" },
        { id: "id184", name: "Snivy", type1: "Water", type2: "", creator: "/u/Aboodie" },
        { id: "id185", name: "Servine", type1: "Water", type2: "", creator: "/u/Aboodie" },
        { id: "id186", name: "Serperior", type1: "Water", type2: "Dragon", creator: "/u/Aboodie" },
        { id: "id187", name: "Chimchar", type1: "Rock", type2: "Psychic", creator: "/u/Aboodie" },
        { id: "id188", name: "Monferno", type1: "Rock", type2: "Psychic", creator: "/u/Aboodie" },
        { id: "id189", name: "Infernape", type1: "Rock", type2: "Psychic", creator: "/u/Aboodie" },
        { id: "id190", name: "Zangoose", type1: "Ice", type2: "Fighting", creator: "/u/SketchyGamer" },
        { id: "id191", name: "Shroomish", type1: "Steel", type2: "", creator: "/u/AChosen_Username" },
        { id: "id192", name: "Breloom", type1: "Steel", type2: "", creator: "/u/AChosen_Username" },
        { id: "id193", name: "Celebi", type1: "Dark", type2: "Fairy", creator: "/u/MrWhat0" },
        { id: "id194", name: "Tropius", type1: "Bug", type2: "Dragon", creator: "/u/Lucci85" },
        { id: "id195", name: "Numel", type1: "Poison", type2: "Dark", creator: "/u/bradleyinabox" },
        { id: "id196", name: "Camerupt", type1: "Poison", type2: "Dark", creator: "/u/bradleyinabox" },
        { id: "id196m", name: "Camerupt", type1: "Poison", type2: "Dark", creator: "/u/bradleyinabox" },
        { id: "id197", name: "Shuppet", type1: "Normal", type2: "Fairy", creator: "/u/Natias257" },
        { id: "id198", name: "Banette", type1: "Normal", type2: "Fairy", creator: "/u/Natias257" },
        { id: "id199", name: "Voltorb", type1: "Water", type2: "Bug", creator: "/u/Lime_Slime64" },
        { id: "id200", name: "Electrode", type1: "Water", type2: "Bug", creator: "/u/Lime_Slime64" }, 
        { id: "id201", name: "Igglybuff", type1: "Fire", type2: "", creator: "/u/mushgloom" },
        { id: "id202", name: "Jigglypuff", type1: "Fire", type2: "", creator: "/u/mushgloom" },
        { id: "id203", name: "Wigglytuff", type1: "Fire", type2: "", creator: "/u/mushgloom" },
        { id: "id204", name: "Maractus", type1: "Steel", type2: "", creator: "/u/mushgloom" },
        { id: "id205", name: "Hoothoot", type1: "Ice", type2: "Flying", creator: "/u/Aboodie" },
        { id: "id206", name: "Noctowl", type1: "Ice", type2: "Flying", creator: "/u/Aboodie" },
        { id: "id207", name: "Stunky", type1: "Fire", type2: "Rock", creator: "/u/SerenityFaithful" },
        { id: "id208", name: "Skuntank", type1: "Fire", type2: "Rock", creator: "/u/SerenityFaithful" },
        { id: "id209", name: "Doduo", type1: "Psychic", type2: "", creator: "/u/TheTurtleScream" },
        { id: "id210", name: "Dodrio", type1: "Psychic", type2: "", creator: "/u/TheTurtleScream" },
        { id: "id211", name: "Moltres", type1: "Fighting", type2: "", creator: "/u/Ubodot" },
        { id: "id212", name: "Girafarig", type1: "Ghost", type2: "Dark", creator: "/u/TheTurtleScream" },
        { id: "id213", name: "Uxie", type1: "Fire", type2: "", creator: "/u/TheTurtleScream" },
        { id: "id214", name: "Mesprit", type1: "Electric", type2: "", creator: "/u/TheTurtleScream" },
        { id: "id215", name: "Azelf", type1: "Ice", type2: "", creator: "/u/TheTurtleScream" },
        { id: "id216", name: "Genesect", type1: "Dark", type2: "Ghost", creator: "/u/DialgaTheTimeLord" },
        { id: "id217", name: "Buneary", type1: "Bug", type2: "Poison", creator: "/u/mushgloom" },
        { id: "id218", name: "Lopunny", type1: "Bug", type2: "Poison", creator: "/u/mushgloom" },
        { id: "id219", name: "Meloetta-Magician", type1: "Dark", type2: "Psychic", creator: "/u/mushgloom" },
        { id: "id219_2", name: "Meloetta-Mime", type1: "Dark", type2: "Fairy", creator: "/u/mushgloom" },
        { id: "id220", name: "Meditite", type1: "Fairy", type2: "", creator: "/u/ensentiumseraph" },
        { id: "id221", name: "Medicham", type1: "Fairy", type2: "Ground", creator: "/u/ensentiumseraph" },
        { id: "id221m", name: "Medicham", type1: "Fairy", type2: "Ground", creator: "/u/ensentiumseraph" },
        { id: "id222", name: "Unown-Delta", type1: "Dark", type2: "Ghost", creator: "/u/sceptile95" },
        { id: "id222_2", name: "Unown-Sigma", type1: "Dark", type2: "Ghost", creator: "/u/sceptile95" },
        { id: "id222_3", name: "Unown-Omega", type1: "Dark", type2: "Ghost", creator: "/u/sceptile95" },
        { id: "id222_4", name: "Unown-Gamma", type1: "Dark", type2: "Ghost", creator: "/u/sceptile95" },
        { id: "id223", name: "Foongus", type1: "Ghost", type2: "Dark", creator: "/u/Ubodot" },
        { id: "id224", name: "Amoonguss", type1: "Ghost", type2: "Dark", creator: "/u/Ubodot" },
        { id: "id225", name: "Corsola-Overcast", type1: "Grass", type2: "", creator: "/u/ensentiumseraph" },
        { id: "id225_2", name: "Corsola-Sunshine", type1: "Grass", type2: "", creator: "/u/ensentiumseraph" },

];
var id = '';

var main = function () {

    var selForme = '';
    var lastUpdated = document.lastModified;

    var UpdateBox = document.getElementById("lastUpdated");
    UpdateBox.innerHTML = "Last Updated: " + lastUpdated;

    star = new Image();
    star.src = "HolonDex/star.png";
    star.className = "inInsurgence";
    $(".insurg").append(star);

    if (location.hash) {
        var HashtagName = location.hash;
        var scrollToName = HashtagName.replace('#', '');
        var scrollArray = $.grep(data, function (scroll) { return scroll.name == scrollToName; });
        var scrollObject = scrollArray[0]
        var scrollID = scrollObject.id;
        var HashtagID = "#" + scrollID;
        scrollToElement($(HashtagID));
    }



    $('.delta').click(function () {
        $(this).select();
    });


    $('.bordered tr').mouseover(function () {
        $(this).addClass('highlight');
    }).mouseout(function () {
        $(this).removeClass('highlight');
    });

    $('.formesel').click(function () {
        var formeimg = $(this).attr("src");
        var activeforme = document.getElementById("infoIMG");

        $("#infoIMG").fadeOut(300, function () {
            infoIMG.src = formeimg
            $(this).fadeIn(300);
        });

        var clickedforme = $(this).attr("id");

        if (selForme !== "") {
            selForme.style.border = '';
            selForme.style.opacity = '';
        }
        selForme = document.getElementById(clickedforme);
        selForme.style.border = '2px solid white';
        selForme.style.opacity = '0.5';


        if (clickedforme == "infoforme1IMG") {
            var activeArray = $.grep(data, function (e) { return e.id == id; });
            var activeObject = activeArray[0]

            var infoName = "Delta " + activeObject.name;
            var namebox = document.getElementById("infoName");
            namebox.innerHTML = infoName;

            var infoType1 = activeObject.type1;
            var imgtype1 = "HolonDex/Types/" + infoType1 + ".png"
            var type1box = document.getElementById("type1img");
            type1box.src = imgtype1;

            var infoType2 = activeObject.type2;
            if (infoType2) {
                var imgtype2 = "HolonDex/Types/" + infoType2 + ".png"
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'inline';
                type2box.src = imgtype2;
            }
            else {
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'none';
            }
        }
        else if (clickedforme == "infoforme2IMG") {
            var formeid = id + "_2"

            var activeArray = $.grep(data, function (e) { return e.id == formeid; });
            var activeObject = activeArray[0]

            var infoName = "Delta " + activeObject.name;
            var namebox = document.getElementById("infoName");
            namebox.innerHTML = infoName;

            var infoType1 = activeObject.type1;
            var imgtype1 = "HolonDex/Types/" + infoType1 + ".png"
            var type1box = document.getElementById("type1img");
            type1box.src = imgtype1;

            var infoType2 = activeObject.type2;
            if (infoType2) {
                var imgtype2 = "HolonDex/Types/" + infoType2 + ".png"
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'inline';
                type2box.src = imgtype2;
            }
            else {
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'none';
            }
        }
        else if (clickedforme == "infoforme3IMG") {
            var formeid = id + "_3"

            var activeArray = $.grep(data, function (e) { return e.id == formeid; });
            var activeObject = activeArray[0]

            var infoName = "Delta " + activeObject.name;
            var namebox = document.getElementById("infoName");
            namebox.innerHTML = infoName;

            var infoType1 = activeObject.type1;
            var imgtype1 = "HolonDex/Types/" + infoType1 + ".png"
            var type1box = document.getElementById("type1img");
            type1box.src = imgtype1;

            var infoType2 = activeObject.type2;
            if (infoType2) {
                var imgtype2 = "HolonDex/Types/" + infoType2 + ".png"
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'inline';
                type2box.src = imgtype2;
            }
            else {
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'none';
            }
        }
        else if (clickedforme == "infoforme4IMG") {
            var formeid = id + "_4"

            var activeArray = $.grep(data, function (e) { return e.id == formeid; });
            var activeObject = activeArray[0]

            var infoName = "Delta " + activeObject.name;
            var namebox = document.getElementById("infoName");
            namebox.innerHTML = infoName;

            var infoType1 = activeObject.type1;
            var imgtype1 = "HolonDex/Types/" + infoType1 + ".png"
            var type1box = document.getElementById("type1img");
            type1box.src = imgtype1;

            var infoType2 = activeObject.type2;
            if (infoType2) {
                var imgtype2 = "HolonDex/Types/" + infoType2 + ".png"
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'inline';
                type2box.src = imgtype2;
            }
            else {
                var type2box = document.getElementById("type2img");
                type2box.style.display = 'none';
            }
        }
        else {
            console.log("Does not exist");
        }
    })
};

$.fn.select = function () {
    var selForme = '';


    id = $(this).attr('id');
    activeArray = [];
    infoType2 = "";

    img = "HolonDex/img/" + id + ".png";
    var infoIMG = document.getElementById("infoIMG");

    $("#activeDelta").fadeOut(300, function () {
        infoIMG.src = img;
        $(this).fadeIn(300);
    });


    if (selForme !== "") {
        selForme.style.border = '';
        selForme.style.opacity = '';
    }

    if ($(this).hasClass("insur")) {
        var insurgencebox = document.getElementById("insurgencebox");
        insurgencebox.style.display = "";

        var insursepar = document.getElementById("insursepar");
        insursepar.style.display = "";
    }
    else {
        var insurgencebox = document.getElementById("insurgencebox");
        insurgencebox.style.display = "none";

        var insursepar = document.getElementById("insursepar");
        insursepar.style.display = "none";
    }


    if ($(this).hasClass("mega")) {
        var megaid = id + "m";
        megaimg = "HolonDex/img/" + megaid + ".png";
        var megaIMG = document.getElementById("infomegaIMG");
        megaIMG.src = megaimg;
    }
    else {
        var megaIMG = document.getElementById("infomegaIMG");
        megaIMG.src = "HolonDex/img/mega.png";
    }

    var activeArray = $.grep(data, function (e) { return e.id == id; });

    var activeObject = activeArray[0]

    var infoName = "Delta " + activeObject.name;
    var namebox = document.getElementById("infoName");
    namebox.innerHTML = infoName;
    if ($(this).hasClass("mega")) {
        var megaactiveArray = $.grep(data, function (e) { return e.id == megaid; });
        var megaactiveObject = megaactiveArray[0]

        var megainfoName = "Mega Delta " + megaactiveObject.name;
        var meganamebox = document.getElementById("megainfoName");
        meganamebox.innerHTML = megainfoName;

        meganamebox.style.display = "";
    }
    else {
        var meganamebox = document.getElementById("megainfoName");
        meganamebox.style.display = "none";
    }

    var infoType1 = activeObject.type1;
    var imgtype1 = "HolonDex/Types/" + infoType1 + ".png"
    var type1box = document.getElementById("type1img");
    type1box.src = imgtype1;

    if ($(this).hasClass("mega")) {

        var megainfoType1 = megaactiveObject.type1;
        var megaimgtype1 = "HolonDex/Types/" + megainfoType1 + ".png"
        var megatype1img = document.getElementById("megatype1img");
        megatype1img.src = megaimgtype1;

        var megatype1row = document.getElementById("megatype1box");
        megatype1row.style.display = "";

        var megatype1sep = document.getElementById("megasepar");
        megatype1sep.style.display = "";
    }
    else {
        var megatype1row = document.getElementById("megatype1box");
        megatype1row.style.display = 'none';

        var megatype1sep = document.getElementById("megasepar");
        megatype1sep.style.display = "none";
    }


    var infoType2 = activeObject.type2;
    if (infoType2) {
        var imgtype2 = "HolonDex/Types/" + infoType2 + ".png"
        var type2box = document.getElementById("type2img");
        type2box.style.display = 'inline';
        type2box.src = imgtype2;
    }
    else {
        var type2box = document.getElementById("type2img");
        type2box.style.display = 'none';
    }
    if ($(this).hasClass("mega")) {

        var megainfoType2 = megaactiveObject.type2;
        var megaimgtype2 = "HolonDex/Types/" + megainfoType2 + ".png"
        var megatype2img = document.getElementById("megatype2img");
        megatype2img.src = megaimgtype2;
    }


    if ($(this).hasClass("forme4")) {

        forme1img = "HolonDex/img/" + id + ".png";
        forme2img = "HolonDex/img/" + id + "_2.png";
        forme3img = "HolonDex/img/" + id + "_3.png";
        forme4img = "HolonDex/img/" + id + "_4.png";

        var forme1IMG = document.getElementById("infoforme1IMG");
        var forme2IMG = document.getElementById("infoforme2IMG");
        var forme3IMG = document.getElementById("infoforme3IMG");
        var forme4IMG = document.getElementById("infoforme4IMG");

        forme1IMG.src = forme1img;
        forme2IMG.src = forme2img;
        forme3IMG.src = forme3img;
        forme4IMG.src = forme4img;

        selForme = document.getElementById("infoforme1IMG");
        selForme.style.border = '2px solid white';
        selForme.style.opacity = '0.5';
    }
    else if ($(this).hasClass("forme2")) {
        forme1img = "HolonDex/img/" + id + ".png";
        forme2img = "HolonDex/img/" + id + "_2.png";

        var forme1IMG = document.getElementById("infoforme1IMG");
        var forme2IMG = document.getElementById("infoforme2IMG");
        var forme3IMG = document.getElementById("infoforme3IMG");
        var forme4IMG = document.getElementById("infoforme4IMG");

        forme1IMG.src = forme1img;
        forme2IMG.src = forme2img;

        selForme = document.getElementById("infoforme1IMG");
        selForme.style.border = '2px solid white';
        selForme.style.opacity = '0.5';
    }
    else {
        var forme1IMG = document.getElementById("infoforme1IMG");
        var forme2IMG = document.getElementById("infoforme2IMG");
        var forme3IMG = document.getElementById("infoforme3IMG");
        var forme4IMG = document.getElementById("infoforme4IMG");

        forme1IMG.src = "HolonDex/img/mega.png";
        forme2IMG.src = "HolonDex/img/mega.png";
        forme3IMG.src = "HolonDex/img/mega.png";
        forme4IMG.src = "HolonDex/img/mega.png";

        selForme1 = document.getElementById("infoforme1IMG");
        selForme1.style.border = '';
        selForme1.style.opacity = '';
        selForme2 = document.getElementById("infoforme2IMG");
        selForme2.style.border = '';
        selForme2.style.opacity = '';
        selForme3 = document.getElementById("infoforme3IMG");
        selForme3.style.border = '';
        selForme3.style.opacity = '';
        selForme4 = document.getElementById("infoforme4IMG");
        selForme4.style.border = '';
        selForme4.style.opacity = '';
    }



    var creator = 'Created by <a href="https://www.reddit.com' + activeObject.creator + '">' + activeObject.creator;
    var creatorbox = document.getElementById("creatorbox")
    creatorbox.innerHTML = creator;
};

$(document).ready(main);

$(document).keydown(throttle(function (e) {
    var lastobject = data[data.length - 1];
    var lastid = lastobject.id;

    if (e.keyCode == 39) {
        if (id && !(id == lastid)) {
            numstr = id.replace('id', '');
            numint = parseInt(numstr);
            newnumint = numint + 1;
            newnumstr = newnumint.toString();
            if (newnumstr.length == 1) {
                newid = "id00" + newnumstr;
            }
            else if (newnumstr.length == 2) {
                newid = "id0" + newnumstr;
            }
            else if (newnumstr.length == 3) {
                newid = "id" + newnumstr;
            }
            id = newid;
            selid = document.getElementById(id)
            $(selid).select();
        }
        else if (!id) {
            selid = document.getElementById("id001")
            $(selid).select();
        }
    }
    if (e.keyCode == 37) {
        if (id && !(id == "id001")) {
            numstr = id.replace('id', '');
            numint = parseInt(numstr);
            newnumint = numint - 1;
            newnumstr = newnumint.toString();
            if (newnumstr.length == 1) {
                newid = "id00" + newnumstr;
            }
            else if (newnumstr.length == 2) {
                newid = "id0" + newnumstr;
            }
            else if (newnumstr.length == 3) {
                newid = "id" + newnumstr;
            }
            id = newid;
            selid = document.getElementById(id)
            $(selid).select();
        }
        else if (!id) {
            selid = document.getElementById("id001")
            $(selid).select();
        }
    }

}, 50));

function throttle(f, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function () {
            f.apply(context, args);
        },
        delay || 500);
    };
}

function scrollToElement(ele) {
    eleYloc = ele.offset().top;
    scrollYloc = eleYloc - 55;
    $(window).scrollTop(scrollYloc).scrollLeft(ele.offset().left);
}